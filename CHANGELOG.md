<!-- @formatter:off -->
# Changelog

All notable changes to this project will be documented in this file.

## [%0.1.0] - 2022-08-14

### 🔨 Miscellaneous Tasks

- [[`5e23eaf7`](https://gitlab.com/michewl/midnight-theme/kitty/-/commit/5e23eaf7ebb72a1fe2a6f5a6ca881e52e26c41d1)] Initial commit

<!-- @formatter:on -->
