[changelog]
# changelog header
header = """
<!-- @formatter:off -->
# Changelog\n
All notable changes to this project will be documented in this file.\n
"""
# template for the changelog body
# https://tera.netlify.app/docs/#introduction
body = """
{% if version %}\
    ## [%{{ version | trim_start_matches(pat="v") }}] - {{ timestamp | date(format="%Y-%m-%d") }}
{% else %}\
    ## [unreleased]
{% endif %}\
{% if previous and previous.commit_id %}\
    \n_See what changed: [{{ previous.version }}...{{ version }}](https://gitlab.com/michewl/midnight-theme/kitty/-/compare/{{ previous.version }}...{{ version }})_
{% endif %}\
{% for group, commits in commits | group_by(attribute="group") %}
    ### {{ group | upper_first }}
    {% for commit in commits %}
        - {% if version and commit.id %}[[`{{ commit.id | truncate(length=8, end="") }}`](https://gitlab.com/michewl/midnight-theme/kitty/-/commit/{{ commit.id }})] {% endif %}{% if commit.scope %}_({{ commit.scope }}):_ {% endif %}{{ commit.message | upper_first }}\
        {% if commit.breaking and commit.breaking_description and commit.breaking_description != commit.message %}\n{% raw %}    - **💥 BREAKING:** {% endraw %}{{ commit.breaking_description | upper_first }}{% endif %}\
    {% endfor %}
{% endfor %}\n
"""
# remove the leading and trailing whitespace from the template
trim = true
# changelog footer
footer = "<!-- @formatter:on -->\n"

[git]
# parse the commits based on https://www.conventionalcommits.org
conventional_commits = true
# filter out the commits that are not conventional
filter_unconventional = false
# regex for preprocessing the commit messages
commit_preprocessors = [
    { pattern = "\\(#([0-9]+)\\)", replace = "([#${1}](https://gitlab.com/michewl/midnight-theme/kitty/-/issues/${1}))" },
    { pattern = "(.*)\\n$", replace = "${1}" }
]
# regex for parsing and grouping commits
commit_parsers = [
    { message = "^build", group = "🔧 Build System and Dependencies" },
    { message = "^ci", group = "👷 CI" },
    { message = "^docs", group = "📝 Documentation" },
    { message = "^feat", group = "✨ Features" },
    { message = "^fix", group = "🐛 Bug Fixes" },
    { message = "^perf", group = "⚡️ Performance" },
    { message = "^refactor", group = "♻️ Refactor" },
    { message = "^chore\\(release\\): prepare for", skip = true },
    { message = "^chore", group = "🔨 Miscellaneous Tasks" },
    { message = "^style", group = "🎨 Codestyle" },
    { message = "^test", group = "✅ Testing" },
    { message = "^revert", group = "⏪️ Revert" },
    { body = ".*security", group = "🔒️ Security" },
    { message = "^Merge", skip = true },
    { message = ".*", group = "Other" }
]
# filter out the commits that are not matched by commit parsers
filter_commits = true
# glob pattern for matching git tags
tag_pattern = "[0-9]*.*"
# regex for skipping tags
skip_tags = ""
# regex for ignoring tags
ignore_tags = ""
# sort the tags chronologically
date_order = false
# sort the commits inside sections by oldest/newest order
sort_commits = "oldest"
